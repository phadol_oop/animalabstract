/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Bat");
        Plane plane = new Plane("PN-01");
        Dog dog = new Dog("Dang");
        
        Flyable[] flyables = {bat, plane};
        for(Flyable f : flyables) {
            if(f instanceof Plane){
                Plane p = (Plane) f;
                p.startEngine();
                p.accelerate();
                p.run();
            }
            f.fly();
        }
        
        Runnable[] runnables = {dog, plane};
        for(Runnable r : runnables) {
            r.run();
        }
    }
}
