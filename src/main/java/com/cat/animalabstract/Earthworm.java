/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Earthworm extends Animal {

    public Earthworm(String name) {
        super(name, 0);
    }

    @Override
    public void eat() {
        System.out.println("Earthworm " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Earthworm " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Earthworm " + getName() + ": sleep.");
    }
}
