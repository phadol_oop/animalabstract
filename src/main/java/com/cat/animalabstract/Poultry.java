/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public abstract class Poultry extends Animal implements Flyable {

    private int numberOfWings;

    public Poultry(String name, int numberOfWings) {
        super(name, 2);
        this.numberOfWings = numberOfWings;
    }

    public int getNumberOfWings() {
        return numberOfWings;
    }

    public void setNumberOfWings(int numberOfWings) {
        this.numberOfWings = numberOfWings;
    }
}
