/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Bat extends Poultry {

    public Bat(String name) {
        super(name, 2);
    }

    @Override
    public void fly() {
        System.out.println("Bat " + getName() + ": fly.");
    }

    @Override
    public void eat() {
        System.out.println("Bat " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Bat " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Bat " + getName() + ": sleep.");
    }
}
