/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public abstract class Vahicle {
    private String engine;

    public Vahicle(String engine) {
        this.engine = engine;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }
    
    public abstract void startEngine();
    public abstract void stopEngine();
    public abstract void accelerate();
    public abstract void applyBreak();
}
