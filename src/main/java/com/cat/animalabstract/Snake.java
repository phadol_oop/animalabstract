/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Snake extends Reptile {

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void crawl() {
        System.out.println("Snake " + getName() + ": crawl.");
    }

    @Override
    public void eat() {
        System.out.println("Snake " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Snake " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Snake " + getName() + ": sleep.");
    }
}
