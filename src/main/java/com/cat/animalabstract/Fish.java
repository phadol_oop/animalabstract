/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Fish extends AquaticAnimals {

    public Fish(String name) {
        super(name, 0);
    }

    @Override
    public void swim() {
        System.out.println("Fish " + getName() + ": swim.");
    }

    @Override
    public void underwaterBreath() {
        System.out.println("Fish " + getName() + ": underwater breath.");
    }

    @Override
    public void eat() {
        System.out.println("Fish " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Fish " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Fish " + getName() + ": sleep.");
    }

}
