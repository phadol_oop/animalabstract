/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Plane extends Vahicle implements Flyable,Runnable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane " + getEngine() + ": start engine.");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane " + getEngine() + ": stop engine.");
    }

    @Override
    public void accelerate() {
        System.out.println("Plane " + getEngine() + ": accelerate.");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane " + getEngine() + ": Break.");
    }

    @Override
    public void fly() {
        System.out.println("Plane " + getEngine() + ": fly.");
    }

    @Override
    public void run() {
        System.out.println("Plane " + getEngine() + ": run.");
    }
}
