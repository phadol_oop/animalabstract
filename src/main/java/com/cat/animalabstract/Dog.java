/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Dog extends TerrestrialAnimals {

    public Dog(String name) {
        super(name, 4);
    }

    @Override
    public void walk() {
        System.out.println("Dog " + getName() + ": walk.");
    }

    @Override
    public void run() {
        System.out.println("Dog " + getName() + ": run.");
    }

    @Override
    public void eat() {
        System.out.println("Dog " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Dog " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Dog " + getName() + ": sleep.");
    }
}
