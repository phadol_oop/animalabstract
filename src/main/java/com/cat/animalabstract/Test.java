/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Test {

    public static void main(String[] args) {
        Human h1 = new Human("AJ DVD");
        Dog d1 = new Dog("Dang");
        Crocodile c1 = new Crocodile("Kee");
        Cat ca1 = new Cat("Maw");
        Fish f1 = new Fish("Pa");
        Crab cr1 = new Crab("Pu");
        Bat b1 = new Bat("Kang");
        Bird bi1 = new Bird("Nok");
        Earthworm e1 = new Earthworm("Sai");
        Snake s1 = new Snake("ngu");
        Animal animals[] = {h1, d1, c1, ca1, f1, cr1, b1, bi1, e1, s1};
        for(int i = 0; i<animals.length; i++){
            if(animals[i] instanceof TerrestrialAnimals){
                TerrestrialAnimals tAni = (TerrestrialAnimals) animals[i];
                tAni.eat();
                tAni.walk();
                tAni.run();
                tAni.speak();
                tAni.sleep();
            }else if(animals[i] instanceof Reptile){
                Reptile rAni = (Reptile) animals[i];
                rAni.crawl();
                rAni.eat();
                rAni.speak();
                rAni.sleep();
            }else if(animals[i] instanceof Poultry) {
                Poultry pAni = (Poultry) animals[i];
                pAni.eat();
                pAni.speak();
                pAni.sleep();
                pAni.fly();
            }else{
                animals[i].eat();
                animals[i].speak();
                animals[i].sleep();
            }
        }
    }
}
