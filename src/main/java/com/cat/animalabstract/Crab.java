/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Crab extends AquaticAnimals {

    public Crab(String name) {
        super(name, 10);
    }

    @Override
    public void swim() {
        System.out.println("Crab " + getName() + ": swim.");
    }

    @Override
    public void underwaterBreath() {
        System.out.println("Crab " + getName() + ": underwaterBreath.");
    }

    @Override
    public void eat() {
        System.out.println("Crab " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Crab " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Crab " + getName() + ": sleep.");
    }
}
