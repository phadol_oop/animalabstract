/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Cat extends TerrestrialAnimals {

    public Cat(String name) {
        super(name, 4);
    }

    @Override
    public void eat() {
        System.out.println("Cat " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Cat " + getName() + ": speak meow meow.");
    }

    @Override
    public void sleep() {
        System.out.println("Cat " + getName() + ": sleep.");
    }

    @Override
    public void walk() {
        System.out.println("Cat " + getName() + ": walk.");
    }

    @Override
    public void run() {
        System.out.println("Cat " + getName() + ": run.");
    }
}
