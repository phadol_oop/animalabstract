/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.animalabstract;

/**
 *
 * @author Black Dragon
 */
public class Crocodile extends Reptile {

    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile " + getName() + ": crawl.");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile " + getName() + ": eat.");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile " + getName() + ": speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile " + getName() + ": sleep.");
    }
}
